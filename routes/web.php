<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Clientes@index');
Route::get('/crear', 'Clientes@crear');
Route::get('/editar/{id}', 'Clientes@editar')->name("editar");
Route::post('/insertar', 'Clientes@insertar')->name("grabar");
Route::put('/update{id}', 'Clientes@update')->name("update");
Route::delete('/eliminar{id}', 'Clientes@eliminar')->name("eliminar");