<?php

namespace App\Http\Controllers;

use App\Cliente; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App; 


class Clientes extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function index(){
	   //cambio desde windows                        
        $data = Cliente::orderBy("id","Desc")->paginate(10);                
        return view('index',compact('data'));
    }

    public function editar($id){
        $data = Cliente::findOrFail($id);  
        //return view('index', ['user' => User::findOrFail($id)]);
        return view('editar',compact('data'));
    }

    public function update(Request $request, $id){
        $clientes       = Cliente::findOrFail($id);         
        $data           = $request->post(); 
        
        $txt_msg        = false;
        //validaciones
        if(strlen(trim($data['nombre'])) == 0){
            $txt_msg = "Ingrese Nombre<br>";
        }                
        if(strlen(trim($data['apellidos'])) == 0){
            $txt_msg.= "Ingrese Apellidos<br>";
        }
        if($txt_msg){
            $_SESSION['msg'] = "<b>ERROR: REGISTRO NO FUE ACTUALIZADO</b>:<br>".$txt_msg;            
            return back()->withInput();
        }
        //end validaciones
        unset($data['_token']);
        unset($data['_method']);        
        foreach((array)$data as $id=>$x){
            $clientes->$id = $x;
        }                
        $_SESSION['msg'] = "Registro Actualizado";    
        $clientes->save(); 
        //return view('index', ['user' => User::findOrFail($id)]);
        return redirect('');
    }

    public function eliminar($id){
        $clientes       = Cliente::findOrFail($id);
        $clientes->delete();
        $_SESSION['msg'] = "Registro Eliminado";
        return redirect('');
    }

    public function crear(Request $request){
        //$data = isset($_SESSION['data_form']) ? $_SESSION['data_form'] : array();
        return view('crear');
    }

    public function insertar(Request $request){        
        $clientes   = New Cliente;        
        $data       = $request->post();
        
        $txt_msg    = false;
        //validaciones
        if(strlen(trim($data['nombre'])) == 0){
            $txt_msg = "Ingrese Nombre<br>";
        }                
        if(strlen(trim($data['apellidos'])) == 0){
            $txt_msg.= "Ingrese Apellidos<br>";
        }
        if($txt_msg){
            $_SESSION['msg'] = "<b>ERROR</b>:<br>".$txt_msg;            
            return back()->withInput();
        }
        //end validaciones
        unset($data['_token']);
        foreach((array)$data as $id=>$x){
            $clientes->$id = $x;
        }            
        $_SESSION['msg'] = "Registro insertado";    
        $clientes->save();
        return redirect('');
    }
}
