@extends ('layout')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Crud Clientes</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Crud</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Filtros</h3>
              <button type="button" class="btn-sm btn-primary float-right" onclick="location.href='{{ url('crear') }}'">Crear Nuevo</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  <th>Apellido(s)</th>
                  <th>Email</th>
                  <th>Fecha Crea</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $time)
                  <tr>
                    <td>{{$time->id}}</td>
                    <td>{{$time->nombre}}</td>
                    <td>{{$time->apellidos}}</td>
                    <td> {{$time->email}}</td>
                    <td><?=date("d-m-Y H:i",strtotime($time->created_at))?></td>
                    <td>
                      
                      <button type="button" class="fa fa-edit btn-sm btn-primary float-left" onclick="location.href='{{ url('editar', $time->id) }}'"></button>
                      <form method="post" action="{{route('eliminar',$time->id)}}">
                        @method('DELETE')
                        @csrf
                        <button type="button" class="fa fa-trash btn-sm btn-danger ml-2" onclick="goDel(this.form,{{ $time->id }})"></button>
                      </form>
                      
                    </td>
                  </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  <th>Apellido(s)</th>
                  <th>Email</th>
                  <th>Fecha Crea</th>
                  <th>Acciones</th>
                </tr>
                </tfoot>
              </table>
              {!! $data->render() !!}
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.4
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://AdminLTE.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

@endsection

<script>
  function goDel(f, id){
    //var url =  "<?=url("eliminar")?>"+"/"+id; 
    //alert(url);   
    if(confirm("Esta seguro de eliminar este registro?")){
      //f.action=url;
      f.submit();
    }
  }
</script>