@extends ('layout')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Crear</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ url('') }}">Inicio</a></li>
            <li class="breadcrumb-item active">Crear</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
  <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Rellene Formulario</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{route('grabar')}}" method="post">              
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nombre (*)</label>
                    <input type="text" name="nombre" value="{{ old('nombre') }}" class="form-control" id="nombre" placeholder="Ingrese">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Apellidos (*)</label>
                    <input type="text" name="apellidos" value="{{ old('apellidos') }}" class="form-control" id="apellidos" placeholder="Ingrese">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
                    <input type="text" name="email" value="{{ old('email') }}" class="form-control" id="email" placeholder="Ingrese">
                  </div>
                                    
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Grabar</button>
                  <button type="button" class="btn btn-warning" onclick="location.href='{{ url('') }}'">Cancelar</button>
                  (*) Requerido
                </div>
              </form>
            </div>
            <!-- /.card -->
    </div>
  </section>
  @endsection